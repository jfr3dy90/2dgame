using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(SpriteRenderer))]
public class CharacterBehaviour : MonoBehaviour
{
    //GetComponentSection
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Animator anim;
    [SerializeField] private SpriteRenderer sprite;

    [SerializeField] private float speedMovement;
    [SerializeField] private Vector2 dir;
    [SerializeField] private float jumpForce;
    
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
       
    }

    private void FixedUpdate()
    {
         
    }


    public void Jump()
    {
       
    }

    void Move(float x)
    {
        dir.x = x*speedMovement;
        rb.velocity = dir;
    }
}
